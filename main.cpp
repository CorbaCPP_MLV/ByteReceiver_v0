#include <iostream>
#include <QApplication>
#include <QFile>
#include <orb_interface.hh>
#include <orb_interface_i.h>

void startServer(int argc, char** argv);

int main(int argc, char** argv)
{
    startServer(argc, argv);

    return 0;
}


void startServer(int argc, char** argv)
{
    try
    {
        // Initialise the ORB.
        CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);

        // Obtain a reference to the root POA.
        CORBA::Object_var obj = orb->resolve_initial_references("RootPOA");
        PortableServer::POA_var poa = PortableServer::POA::_narrow(obj);

        // We allocate the objects on the heap.  Since these are reference
        // counted objects, they will be deleted by the POA when they are no
        // longer needed.
        bytes_transmission_Echo_i* mybytes_transmission_Echo_i = new bytes_transmission_Echo_i();


        // Activate the objects.  This tells the POA that the objects are
        // ready to accept requests.
        PortableServer::ObjectId_var mybytes_transmission_Echo_iid = poa->activate_object(mybytes_transmission_Echo_i);


        // Obtain a reference to each object and output the stringified
        // IOR to stdout
        {
            // IDL interface: bytes_transmission::Echo
            CORBA::Object_var ref = mybytes_transmission_Echo_i->_this();
            CORBA::String_var sior(orb->object_to_string(ref));
            std::cout << "IDL object bytes_transmission::Echo IOR = '" << (char*)sior << "'" << std::endl;

            //------------------------------------------------------------------------
            QString iorPath("/tmp/ior");
            QFile caFile(iorPath);
            caFile.open(QIODevice::WriteOnly | QIODevice::Text);

            if(!caFile.isOpen())
            {
                qDebug() << "- Error, unable to open" << iorPath << "for output";
            }
            QTextStream outStream(&caFile);
            outStream << sior;
            caFile.close();
        }



        // Obtain a POAManager, and tell the POA to start accepting
        // requests on its objects.
        PortableServer::POAManager_var pman = poa->the_POAManager();
        pman->activate();

        orb->run();
        orb->destroy();
    }
    catch(CORBA::TRANSIENT&)
    {
        std::cerr << "Caught system exception TRANSIENT -- unable to contact the "
                  << "server." << std::endl;
    }
    catch(CORBA::SystemException& ex)
    {
        std::cerr << "Caught a CORBA::" << ex._name() << std::endl;
    }
    catch(CORBA::Exception& ex)
    {
        std::cerr << "Caught CORBA::Exception: " << ex._name() << std::endl;
    }
}

