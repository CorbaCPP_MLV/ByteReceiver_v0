//
// Example code for implementing IDL interfaces in file orb_interface.idl
//

#include "orb_interface_i.h"

//
// Example implementation code for IDL interface 'bytes_transmission::Echo'
//
bytes_transmission_Echo_i::bytes_transmission_Echo_i()
{
}
bytes_transmission_Echo_i::~bytes_transmission_Echo_i()
{
}

// Methods corresponding to IDL attributes and operations
char* bytes_transmission_Echo_i::sendBytes(const bytes_transmission::ByteArray& bytes)
{
    QByteArray imgData;
    for(CORBA::ULong i = 0; i < bytes.length(); i++)
    {
        imgData.append(bytes[i]);
    }

    QImage img(415, 311, QImage::Format_Indexed8);
    img = QImage::fromData(imgData);

    //MainWindow::instance.ui->Image->setPixmap(QPixmap::fromImage(img));

    qDebug() << "Image received";
    return CORBA::string_dup("0");
}

// End of example implementation code
