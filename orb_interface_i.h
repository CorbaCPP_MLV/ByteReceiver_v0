#ifndef ORB_INTERFACE_I_H
#define ORB_INTERFACE_I_H

#include <iostream>
#include <QDebug>
#include <QImage>
#include <orb_interface.hh>

//
// Example class implementing IDL interface bytes_transmission::Echo
//
class bytes_transmission_Echo_i : public POA_bytes_transmission::Echo
{
private:
    // Make sure all instances are built on the heap by making the
    // destructor non-public
    //virtual ~bytes_transmission_Echo_i();

public:
    // standard constructor
    bytes_transmission_Echo_i();
    virtual ~bytes_transmission_Echo_i();

    // methods corresponding to defined IDL attributes and operations
    char* sendBytes(const bytes_transmission::ByteArray& bytes);
};

#endif // ORB_INTERFACE_I_H
