TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
QT += core gui widgets printsupport
#CONFIG -= qt
LIBS += -lomniORB4 -lomnithread -lomniDynamic4

SOURCES += main.cpp \
    orb_interface_i.cc \
    orb_interfaceSK.cc

HEADERS += \
    orb_interface.hh \
    orb_interface_i.h

FORMS +=
